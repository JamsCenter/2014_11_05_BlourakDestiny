﻿using UnityEngine;
using System.Collections;

public class BlourakBockInGround : MonoBehaviour {

    public BlourakController blourak;
    public ObjectBlockedInGround blockGround;

    public void Start() {
        if (blourak != null && blockGround!=null) 
        {
            blockGround.onBlockInGround += (p,onOff) => {
                blourak.SetAsBlocked(onOff);
            };
        
        }
        Destroy(this);

    }
}
