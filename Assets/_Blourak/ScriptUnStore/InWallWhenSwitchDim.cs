﻿using UnityEngine;
using System.Collections;

public class InWallWhenSwitchDim : MonoBehaviour {
    
    public Rigidbody2D rigObject;
    public Dimensions switchDimension;
    public Checker blockGroundDetector;
    private Vector3 velocityBeforeCheck;
    
    public delegate void WallOverlapingDetected();
    public  WallOverlapingDetected onOverlapingDetected;

   public void Start()
    {
        if (rigObject == null) rigObject.GetComponent<Rigidbody2D>();
        if (switchDimension == null) switchDimension= Dimensions.Instance;

       if ( blockGroundDetector != null && switchDimension != null && rigObject != null)
       {

           switchDimension.BeforeSwitch += (root, color,from,to) =>
           {
               if (rigObject != null)
               {
                   velocityBeforeCheck = rigObject.velocity;
                   rigObject.isKinematic = true;
               }
           };

           switchDimension.AfterSwitch += (root, color, from, to) =>
           {
                bool blocked = blockGroundDetector.IsColliding2D();


                if (onOverlapingDetected != null && blocked)
                {
                    onOverlapingDetected();
                }
               if (rigObject != null)
                   {
                       rigObject.isKinematic = blocked;
                       if(!blocked)
                           rigObject.velocity = velocityBeforeCheck;
                   }
           };

       }

   }

}


