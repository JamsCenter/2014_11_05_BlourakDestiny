﻿using UnityEngine;
using System.Collections;

public class HasLife : MonoBehaviour {

    public float initalLife=100;
    private float _life;
    public bool hasBeenDeathOnce = false;

    public float Life
    {
        get { return _life; }
        set
        {
            float old = _life;
            if (value < 0f) value = 0f; 
            _life = value;
            if (onLifeChange != null)
                onLifeChange(this, old, _life);
            if (!hasBeenDeathOnce && _life <= 0) 
            {
                hasBeenDeathOnce = true;
                if (onNoLifeAnymore != null)
                    onNoLifeAnymore(this);
            }    
        }
    }
    void Awake() { Life = initalLife; }
    public delegate void HasNoLifeAnymore(HasLife obj);
    public HasNoLifeAnymore onNoLifeAnymore;

    public delegate void LifeChange(HasLife obj, float oldLife, float newLife);
    public LifeChange onLifeChange;


    public void Reset()
    {
        Life = initalLife;
    }

    public float GetPourcentCompareToInital()
    {
        return Life / initalLife;
    }
}
