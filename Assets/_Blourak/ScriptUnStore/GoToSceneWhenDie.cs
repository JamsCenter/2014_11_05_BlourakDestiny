﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(HasLife))]
public class GoToSceneWhenDie : MonoBehaviour {

    private HasLife life;
    public string sceneName = "Menu";	// Use this for initialization
	void Start () {
        life = GetComponent<HasLife>() as HasLife;
        if(life)
        life.onNoLifeAnymore += ChangeSceen;
	}
    public void OnDestroy() {
        if (life)
            life.onNoLifeAnymore -= ChangeSceen;
	
    }
    public void ChangeSceen(HasLife life) 
    {
        Application.LoadLevel(sceneName);
    }
	
	void Update () {
	
	}
}
