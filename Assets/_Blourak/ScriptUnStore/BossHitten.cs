﻿using UnityEngine;
using System.Collections;

public class BossHitten : MonoBehaviour {

    public delegate void BossHit(Vector3 where,Transform who);
    public BossHit onBossHit;
    

    public float minTimeBetweenHitTrigger=1f;
    private float lastTime;
    public void OnCollisionEnter2D(Collision2D col) 
    {
        if (!col.gameObject.tag.Equals("Player")) return;
        float t = Time.time;
        if (t - lastTime < minTimeBetweenHitTrigger) return;
        lastTime = Time.time;          

        print("Hit");
        Vector3 where = new Vector3(col.contacts[0].point.x, col.contacts[0].point.y, transform.position.z);
        if (onBossHit!=null)
            onBossHit(where,col.transform);
        
    
    }
}
