﻿using UnityEngine;
using System.Collections;

public class RandomPositions : Positions {

    public Transform[] positions;
    public Vector3 tmp;
    public int cursor;
    public override Vector3 GetPosition()
    {
        return tmp;
    }

    public override void Next()
    {
        if (positions != null || positions.Length <= 0)
            tmp= transform.position;
        Transform pos = positions[Random.Range(0,positions.Length)];
        tmp= pos == null ? transform.position : pos.position;
    
    }
}
