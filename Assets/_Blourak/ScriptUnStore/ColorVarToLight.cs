﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Light))]
public class ColorVarToLight : MonoBehaviour {

    public ColorChangeWithDim objectColor;
    public Light light;

    void Start() 
    {
        if (light)
            light = GetComponent<Light>() as Light;
        if (objectColor)
            objectColor = GetComponent<ColorChangeWithDim>() as ColorChangeWithDim;
    }

	void Update () {
        light.color = objectColor.currentColor; 
	
	}
}
