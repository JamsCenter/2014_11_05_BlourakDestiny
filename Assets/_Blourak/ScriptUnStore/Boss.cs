﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Boss : MonoBehaviour {

    public BossHitten bossHit;
    public float forceAddWhenHit = 10f;
    public OnHitIgnoreCollisionForWhile ignoreHit;
    public Positions positions;
    public Vector3 nextPosition;
    public float speed = 1f;


    public HasLife life;
    public float lifeLoseWhenHit=10f;
   

    public GameObject bossPorjectilPrefab;
    public float fireMinTime=1f, fireMaxTime=4f;
    void Start () {

        if (!life)
            life = GetComponent<HasLife>() as HasLife;
        if (bossHit != null)
            bossHit.onBossHit += (where, who) =>
            {
                Vector3 dir = where-who.position;
                if(GetComponent<Rigidbody2D>())
                    GetComponent<Rigidbody2D>().AddForce(dir * forceAddWhenHit,ForceMode2D.Impulse);
                if(ignoreHit)
                   ignoreHit.IsHitten(gameObject, who.gameObject);
                if (positions)
                {
                    ChangePosition();

                }
                if (life) life.Life -= lifeLoseWhenHit;

            };
    }
    public void OnEnable(){
        StartCoroutine("FireEvery");
        ChangePosition();
	}

    private void ChangePosition()
    {
        positions.Next();
        nextPosition = positions.GetPosition();
    }

    public IEnumerator FireEvery()
    {
        float nextTime = 1f;
        yield return new WaitForSeconds(nextTime);
        while (true)
        {
            nextTime = Random.Range(fireMinTime,fireMaxTime);
            if (bossPorjectilPrefab)
                Instantiate(bossPorjectilPrefab, transform.position, bossPorjectilPrefab.transform.rotation);

            yield return new WaitForSeconds(nextTime);
        }

    }
	

	void Update () {
        if (Vector3.Distance(transform.position, nextPosition) < 1f) return;
        Vector3 dir= nextPosition-transform.position;
        GetComponent<Rigidbody2D>().velocity = dir.normalized * speed*Time.deltaTime;  
	
    }

}
