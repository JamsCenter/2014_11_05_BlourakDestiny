﻿using UnityEngine;
using System.Collections;

public class BlourakSetAnimation : MonoBehaviour {
    public Animator animator;
    public bool IsWalking
    {
        get { return animator.GetBool("IsWalking"); }
        set {animator.SetBool("IsWalking", value);}
    }

    public bool IsRunning
    {

        get { return animator.GetBool("IsRunning"); }
        set { animator.SetBool("IsRunning", value); }
    }

    public bool HasGround
    {

        get { return animator.GetBool("HasGround"); }
        set { animator.SetBool("HasGround", value); }
    }
    public bool IsBlocked
    {

        get { return animator.GetBool("IsBlocked"); }
        set { animator.SetBool("IsBlocked", value); }
    }

    public void Jump()
    {
        if (animator != null)
        {
            animator.SetTrigger("JumpTrig");
        }
    }
    



}
