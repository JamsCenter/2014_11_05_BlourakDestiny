﻿using UnityEngine;
using System.Collections;

public class LifeToSize : MonoBehaviour {


    public HasLife life;
    public GameObject body;
    public float min = 0, max = 255;
    public float minSize = 0.3f, maxSize =1f;
    public bool inverse;

    public Vector3 initialSize;

    void Start()
    {
        if(body!=null)
        initialSize = body.transform.localScale;
        if (!life)
            life = GetComponent<HasLife>() as HasLife;
        if (life ) { 
        life.onLifeChange += LifeChange;
        life.Reset();
        }



    
    }

    public void LifeChange(HasLife obj, float o, float n)
    {
        n = Mathf.Clamp(n, min, max);
        float pourcent = n / max;
        if (inverse)
            pourcent = 1f - pourcent;

        if (body)
            body.transform.localScale = initialSize * (minSize + pourcent * (maxSize - minSize));
       
    }

    void OnDestroy()
    {
        life.onLifeChange -= LifeChange;
    }
}
