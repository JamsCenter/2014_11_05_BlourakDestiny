﻿using UnityEngine;
using System.Collections;

public class InputSwitchDim : MonoBehaviour {


    public Dimensions dimension;
    public KeyCode[] red= new KeyCode[]{KeyCode.R };
    public KeyCode[] green = new KeyCode[] { KeyCode.G };
    public KeyCode[] blue = new KeyCode[] { KeyCode.B };
    public KeyCode[] black = new KeyCode[] { KeyCode.X };
    public KeyCode[] white = new KeyCode[] { KeyCode.W };


	void Update () {


        if (CheckIfSwitchDim(Dimensions.DimType.White, white)) return;
        if (CheckIfSwitchDim(Dimensions.DimType.Black, black)) return;
        if (CheckIfSwitchDim(Dimensions.DimType.Blue, blue)) return;
        if (CheckIfSwitchDim(Dimensions.DimType.Green, green)) return;
        if (CheckIfSwitchDim(Dimensions.DimType.Red, red)) return;
       
	
	}

    private bool CheckIfSwitchDim(Dimensions.DimType type, KeyCode[] keycodes)
    {
        if(dimension==null)return false;
        foreach (KeyCode kc in keycodes)
            if (Input.GetKeyDown(kc))
            {
                dimension.SwitchDimension(type);
                return true;
            }
        return false;
    }
}
