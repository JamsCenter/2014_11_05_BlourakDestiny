﻿using UnityEngine;
using System.Collections;

public class CameraTriggerFocus : CameraFocusElement {

    public float priotityFocus = 10f;
    public float size = 10f;


    public void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.gameObject.tag.Equals("Player")) return;
        CameraFocus.AddFocus(this);
    }

    public void OnTriggerExit2D(Collider2D col)
    {
        if (!col.gameObject.tag.Equals("Player")) return;
        CameraFocus.RemoveFocus(this);
    }


    public override Vector3 GetCameraPosition()
    {
        return transform.position;
    }

    public override float GetCameraSize()
    {
        return size;
    }

    public override float GetPriority()
    {
        return priotityFocus;
    }
}
