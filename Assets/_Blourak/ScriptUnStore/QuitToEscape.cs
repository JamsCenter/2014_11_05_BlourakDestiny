﻿using UnityEngine;
using System.Collections;

public class QuitToEscape : MonoBehaviour {


    public string restartLevel = "Game";
	void Start () {
	
	}
	
	// Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();

        if (Input.GetKeyDown(KeyCode.Delete))
            Application.LoadLevel(restartLevel);
	}
}
