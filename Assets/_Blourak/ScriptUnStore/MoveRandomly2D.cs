﻿using UnityEngine;
using System.Collections;
using be.primd.blourak;

[RequireComponent(typeof(Rigidbody2D))]
public class MoveRandomly2D : MonoBehaviour {

    public Vector3 nextPosition;
    public TrackedPoint trackedPoint;
    public bool move=true;
    public float fromChangeDirTime = 1f;
    public float toChangeDirTime = 10f;
    public float checkCollisionEvery = 0.25f;
    public float speed=1f;


    private float countDown;
    private float countDownSecond;
    public LayerMask wallLayers;
    public float distanceCollChange=1f;

    private Vector3 direction;
	// Use this for initialization
	void Start () {
        ChangeDirection();
    }

	
	// Update is called once per frame
	void Update () {

        if (!move) return;
        if (countDown > 0f)
        {
            countDown -= Time.deltaTime;

            if (countDown < 0f) 
            {
                countDown = 0f;
                ChangeDirection();
            }
        }

        countDownSecond -= Time.deltaTime;
        
            if (countDownSecond <= 0f)
            {
                countDownSecond = checkCollisionEvery;
                 Vector3 pos =transform.position;
                 Vector3 dir = direction = (nextPosition - transform.position).normalized;
                 Vector3 next =pos+dir*distanceCollChange;
                RaycastHit2D hit = Physics2D.Linecast(pos,next, wallLayers);
                if (hit)
                {
                    Vector3 collDir = (hit.transform.position - transform.position).normalized;
                    bool right = !(collDir.x > 0f && dir.x > 0);
                    bool top = !(collDir.y > 0f && dir.y > 0);
                    GetComponent<Rigidbody2D>().velocity /= 2f;
                    ChangeDirection(right,top);
                   // ChangeDirection(dir.x>0f,dir.y>0f);
                }
            }
        

        GetComponent<Rigidbody2D>().AddForce((nextPosition - transform.position).normalized*speed*Time.deltaTime, ForceMode2D.Impulse);
        CheckModelDirection();
	}

    private void CheckModelDirection()
    {
        if (direction.x > 0 && transform.localScale.x < 0f)
        {
            Vector3 newScale = transform.localScale;
            newScale.x = Mathf.Abs(newScale.x);
            transform.localScale = newScale;
            //OnSwitchDirection right
        }
        else if (direction.x < 0 && transform.localScale.x > 0f)
        {
            Vector3 newScale = transform.localScale;
            newScale.x = -Mathf.Abs(newScale.x);
            transform.localScale = newScale;
            //OnSwitchDirection left
        }
    }
    private void ChangeDirection(bool right, bool top)
    {
        float maxRange = 1000f;
        nextPosition.x = Random.Range(!right ? -maxRange : 0f, right ? maxRange : 0f);
        nextPosition.y = Random.Range(!top ? -maxRange : 0f, top ? maxRange : 0f);
        countDown = Random.Range(fromChangeDirTime, toChangeDirTime);
        
    }

    private void ChangeDirection()
    {
        float maxRange = 1000f;
        nextPosition.x = Random.Range(-maxRange ,maxRange );
        nextPosition.y = Random.Range( -maxRange , maxRange );
        countDown = Random.Range(fromChangeDirTime, toChangeDirTime);
    }


    public void OnCollisionEnter2D(Collision2D col) 
    {
        Vector3 dir = transform.position -col.transform.position;
        ChangeDirection(dir.x>0f,dir.y>0f);
            
    }

}
