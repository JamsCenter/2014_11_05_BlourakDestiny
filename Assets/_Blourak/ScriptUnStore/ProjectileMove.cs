﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class ProjectileMove : MonoBehaviour {
    
    public Transform target;
    public Vector3 direction=Vector3.right;
    public float speed=1f;
    public float acceleration=1f;
    public float maxSpeed = 5f;

    public bool move=true;
    public bool autoFocusPlayer;
    public float toFarDistance=5;
    private Vector3 toFarDirection;
    private bool toFar;

    public bool ToFar
    {
        get { return toFar; }
        set {
            if (!toFar && value)
            {
                if (!target)
                {
                    toFarDirection.x = Random.RandomRange(-1000f, 1000f);
                    toFarDirection.y = Random.RandomRange(-1000f, 1000f);
                    toFarDirection.Normalize();
                }
                //if (target)
                //{
                //    toFarDirection = target.position - transform.position;
                //    toFarDirection.x *= Random.RandomRange(-1f, 5f);
                //    toFarDirection.y *= Random.RandomRange(-5f, 5f);
                //    toFarDirection.Normalize();
                //}
            }
            toFar = value;
        
        }
    }

    void Start()
    {
        if (autoFocusPlayer && target==null)
        {
            GameObject gamo = GameObject.FindWithTag("Player") as GameObject;
            target = gamo.transform;
            if (target != null) move=true;
        }
    }

    void Update() 
    {
        if (!move || !target || ! target.gameObject.activeInHierarchy) return;
        speed += acceleration * Time.deltaTime;
        speed = Mathf.Clamp(speed, 0f, maxSpeed);
        if (target) {

            float dTarget = Vector3.Distance(transform.position, target.position);
            ToFar = dTarget > toFarDistance;
            

            Vector3 targetDir = (target.position - transform.position).normalized;
            //if (ToFar)
            //{
            //    targetDir += toFarDirection.normalized*0.4f;
                
            //} 
            direction = Vector3.Lerp(direction, targetDir, Time.deltaTime);
           // dir += targetDir / 2f;
            
        }

        GetComponent<Rigidbody2D>().velocity = direction.normalized * speed;


    
    }

}
