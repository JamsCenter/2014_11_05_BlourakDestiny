﻿using UnityEngine;
using System;

namespace be.primd.blourak
{
    interface ITrackedPoint
    {
        Vector3 Direction { get; }
        Vector3 Position { get; }
        Quaternion Rotation { get; }
        float Speed { get; }
    }
}
