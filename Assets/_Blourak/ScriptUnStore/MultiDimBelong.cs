﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MultiDimBelong : MonoBehaviour {


    public Dimensions.DimType origineDim;
    public bool addAtStart=true;
    public List<Dimensions.DimType> belongDim=new List<Dimensions.DimType>();

    public delegate void BelongToNewDimension(MultiDimBelong who, Dimensions.DimType newDim, Dimensions.DimColor newColor);
    public BelongToNewDimension onBelongToNewDimension;

    public delegate void LeaveDimension(MultiDimBelong who, Dimensions.DimColor newColor);
    public LeaveDimension onLeaveDimension;
    public bool stopExisting;
    void Start() {
        if (addAtStart)
        AddTo(origineDim);
    }

    public Dimensions.DimType GetOriginalDimension() { return origineDim; }
    public Dimensions.DimType [] GetBelongTo() { return belongDim.ToArray(); }

    public Dimensions.DimColor GetDimensionColor()
    {
       return Dimensions.GetDimensionColor(belongDim);
    }

    public void AddTo(Dimensions.DimType dim)
    {
        if (BelongTo(dim)) return;
        belongDim.Add(dim);
        if (onBelongToNewDimension != null)
            onBelongToNewDimension(this, dim, Dimensions.GetDimensionColor(belongDim));
    }
    public void RemoveFrom(Dimensions.DimType dim)
    {
        if (!BelongTo(dim)) return;
        belongDim.Remove(dim);
        if (onLeaveDimension != null)
            onLeaveDimension(this, Dimensions.GetDimensionColor(belongDim));

        if (onBelongToNewDimension != null)
            onBelongToNewDimension(this, dim, Dimensions.GetDimensionColor(belongDim));
    }

    public bool BelongTo(Dimensions.DimType dim)
    {
        return belongDim.Contains(dim);
    }

    public void SetAsOnly(Dimensions.DimType dimType)
    {
        ResetDim();
        AddTo(dimType);
    }

    public void ResetDim()
    {

        foreach (Dimensions.DimType dt in belongDim.ToArray())
            RemoveFrom(dt);
    }



    public void AddDim(MultiDimBelong dimElemt)
    {
        if (dimElemt)
            foreach (Dimensions.DimType dt in dimElemt.GetBelongTo())
                AddTo(dt);

    }

    internal void AddRandomRGB()
    {
        int rand = Random.Range(0, 3);
        if (rand == 0) AddTo(Dimensions.DimType.Red);
        else if (rand == 1) AddTo(Dimensions.DimType.Green);
        else if (rand == 2) AddTo(Dimensions.DimType.Blue);

    }

    public void StopExisting(bool value)
    {
        stopExisting = value;
    }   

    public bool IsExisting()
    {
        return !stopExisting;
    }
}
