﻿using UnityEngine;
using System.Collections;

public class ActiveOnlyInGoodDim : MonoBehaviour {

    public MultiDimBelong multiDim;
    public Dimensions dimension;
    public bool inverse;
	void Start () {
        if (!multiDim){ Destroy(this); return; }
        if(!dimension){ dimension= Dimensions.Instance;}
        if(!dimension){ Destroy(this); return;}

        Activate(Dimensions.Instance.currentDimension);
        dimension.OnSwitch += OnSwitchSetActice;
	
	}
    public void OnDestroy() 
    {
        if (dimension)
        {
            dimension.OnSwitch -= OnSwitchSetActice;
        }
    }
    private void OnSwitchSetActice(GameObject root, Color color, Dimensions.DimType o, Dimensions.DimType n)
    {
        if (gameObject && multiDim)
        {
            if (inverse)
                gameObject.SetActive(!multiDim.BelongTo(n));
            else Activate(n);
        }
    }
    private void Activate(Dimensions.DimType n)
    {
        if (multiDim && !multiDim.IsExisting()) { 
            gameObject.SetActive(false);
            return;    
        }
        if (inverse)
            gameObject.SetActive(!multiDim.BelongTo(n));
        else gameObject.SetActive(multiDim.BelongTo(n));
    }
	
	void Update () {
	
	}
}
