﻿using UnityEngine;
using System.Collections;

public class IgnoreLayer : MonoBehaviour {


    public string[] layersName;
    public bool  _2D;
    public bool  _3D;
    public bool ignore = true;
	// Use this for initialization
    void Awake() 
    {
            int layA = gameObject.layer;
        foreach (string layer in layersName)
        {
            int layB = LayerMask.NameToLayer(layer);
            if (!(layB >= 0 && layB <= 31)) continue;
            if (_2D)
                Physics2D.IgnoreLayerCollision(layA, layB, ignore);
            if (_3D)
                Physics.IgnoreLayerCollision(layA, layB, ignore);
        }
    
    }
}
