﻿using UnityEngine;
using System.Collections;

public class DimensionStation : MonoBehaviour {

    public Dimensions.DimType dimension;

    void OnTriggerEnter2D(Collider2D col) 
    {
        //print("Coll enter "+dimension);
        if (col.gameObject.tag.Equals("Projectile")) 
        {
            MultiDimBelong dim = col.gameObject.GetComponent<MultiDimBelong>() as MultiDimBelong;
            if (dim) dim.AddTo(dimension);
            
        }
    }
}
