﻿using UnityEngine;
using System.Collections;

public class BossFocus : CameraFocusElement
{
    public float priotityFocus = 10f;

    public float distanceMax = 20f;
    public float sizeMax = 20f;

    public float distanceMin = 1f;
    public float sizeMin = 10f;

    public Dimensions dimensions;
    public Transform player;
    public float pourcent=0.6f;

    public void Start()
    {
        if(dimensions)
        dimensions.OnSwitch += AddFocusBetweenPlayerAndBoss;
        if (dimensions)
        dimensions.OnSwitch += RemoveFocusBetweenPlayerAndBoss;

        if (!player)
        { 
        GameObject gamo = GameObject.FindWithTag("Player");
        if (gamo != null) player = gamo.transform;
        }
}
    public void OnDestroy()
    {
        if(dimensions)
        {
        dimensions.OnSwitch -= AddFocusBetweenPlayerAndBoss;
        dimensions.OnSwitch -= RemoveFocusBetweenPlayerAndBoss;}
    }

    public void AddFocusBetweenPlayerAndBoss(GameObject root, UnityEngine.Color color, Dimensions.DimType from, Dimensions.DimType to)
    {
        if(to == Dimensions.DimType.Black)
        CameraFocus.AddFocus(this);
    }
    public void RemoveFocusBetweenPlayerAndBoss(GameObject root, UnityEngine.Color color, Dimensions.DimType from, Dimensions.DimType to)
    {
        if (!(to == Dimensions.DimType.Black))
        CameraFocus.RemoveFocus(this);
    }


    public override Vector3 GetCameraPosition()
    {
        if (player == null)
            return transform.position;
        else
            return transform.position + (player.position - transform.position) * pourcent;
    }

    public override float GetCameraSize()
    {
        float d = Vector3.Distance(player.position, transform.position);

        d = d - distanceMin;
        float pourcent =(d-distanceMin)/ (distanceMax-distanceMin);
        
        return sizeMin+ (sizeMax-sizeMin)*pourcent;
    }

    public override float GetPriority()
    {
        return priotityFocus;
    }
}
