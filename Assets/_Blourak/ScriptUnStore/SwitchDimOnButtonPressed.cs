﻿using UnityEngine;
using System.Collections;

public class SwitchDimOnButtonPressed : MonoBehaviour {

    public Dimensions.DimType dimension;

    public void SwitchDim() 
    {
        if (Dimensions.Instance) 
        {
            Dimensions.Instance.SwitchDimension(dimension);
        }
    }
}
