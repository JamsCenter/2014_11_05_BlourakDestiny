﻿using UnityEngine;
using System.Collections;

public class LifeToAlpha : MonoBehaviour {


    public HasLife life;
    public Material material;
    public float min = 0f, max = 255f;
    public bool inverse;

    void Start()
    {
        if (!life)
            life = GetComponent<HasLife>() as HasLife;
        if (life && material) { 
        life.onLifeChange += (obj, o, n) =>
        {
            n = Mathf.Clamp(n, min, max);
            float pourcent = n / max;
            if (inverse)
                pourcent = 1f - pourcent;

            material.SetColor("_Color",new Color(pourcent, pourcent, pourcent, 1f));

        };

        life.Reset();
        }


    
    }
}
