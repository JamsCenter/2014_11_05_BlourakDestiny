﻿using UnityEngine;
using System.Collections;

public class OnHitIgnoreCollisionForWhile : MonoBehaviour {


    public float ignoreTime = 5f;
    public bool withCollision=true;
    public bool withTrigger = true;


    public string [] workWithTag;

    public bool  ignoreCol;

    private GameObject _hitten;

    public GameObject Hitten
    {
         get { return _hitten; }
        private set
        {
            if (!ignoreCol)
                _hitten = value;
        }
    } 
    private GameObject _hitter;
    public GameObject Hitter
    {
         get { return _hitter; }
        private set
        {
            if (!ignoreCol)
                _hitter = value;
        }
    }
    


    public void IsHitten(GameObject hitter, GameObject hitten) 
    {
        if (ignoreCol) return;
        if (!hitten || !hitter) return;
            Hitten = hitten;
            Hitter = hitter;
            Physics2D.IgnoreLayerCollision(Hitter.layer, Hitten.layer, true);
            ignoreCol = true;
            Invoke("Recollide",ignoreTime);
    }
    public void Recollide() 
    {
        Physics2D.IgnoreLayerCollision(Hitter.layer, Hitten.layer, false);
        ignoreCol = false;
    }


    public void OnTriggerEnter2D(Collider2D col)
    {
        if (!withTrigger) return;
        if (workWithTag==null || workWithTag.Length <= 0) return;
        foreach (string t in workWithTag)
            if (t == null || !t.Equals(col.gameObject.tag)) return;
        IsHitten(gameObject, col.gameObject);
    }
    public void OnCollisionEnter2D(Collision2D col)
    {
        if (!withCollision) return;
        if (workWithTag==null || workWithTag.Length <= 0) return;
        foreach (string t in workWithTag)
            if (t == null || !t.Equals(col.gameObject.tag)) return;
        IsHitten(gameObject, col.gameObject);
    }
}
