﻿using UnityEngine;
using System.Collections;

public class EnableOnlyInGoodDim : MonoBehaviour {


    public MonoBehaviour script;
    public MultiDimBelong multiDim;
    public Dimensions dimension;
    void Start()
    {
        if (!multiDim) { Destroy(this); return; }
        if (!dimension) { dimension = Dimensions.Instance; }
        if (!dimension) { Destroy(this); return; }
        script.enabled=multiDim.BelongTo(Dimensions.Instance.currentDimension);
        dimension.OnSwitch += (root, color, o, n) =>
        {
            script.enabled =multiDim.BelongTo(n);

        };

    }

    void Update()
    {

    }
}
