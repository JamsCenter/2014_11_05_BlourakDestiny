﻿using UnityEngine;
using System.Collections;

public abstract class Positions : MonoBehaviour {

    public abstract Vector3 GetPosition();
    public abstract void Next();	
}

